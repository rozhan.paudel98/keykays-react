import React, { useState, useEffect } from "react";

import { Link } from "react-router-dom";
import { useHistory } from "react-router-dom";
import axios from "axios";
export default function Header({ shouldDisplay }) {

const history = useHistory();
  const [count, setCount] = useState(0);
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [searchItems, setSearchItems] = useState([]);
  const [searchDisplayItems, setSearchDisplayItems] = useState([]);
  const [currency, setCurrency] = useState("USD");
  //code for currency change everywhere
  const [aud, setAud] = useState("");
  const [cad, setCad] = useState("");
  //code for currency change everywhere

  const handleSearchQuery = async (query) => {
    var products = [];
    console.log("Header search bar products", products);
    searchItems.forEach((elem) => {
      if (elem.productName.toLowerCase().includes(query.toLowerCase()))
        products.push(elem);
    });
    console.log(products);
    console.log("Header search bar products", products);
    setSearchDisplayItems(products);
    if (!query) {
      setSearchDisplayItems([]);
    }
  };

  const handleLogout = () => {
    localStorage.removeItem("token");
    setIsLoggedIn(false);
    history.push("/");
  };

  const handleCurrencyChange = (value) => {
    console.log(value);
    setCurrency(value);
    localStorage.setItem("currency", value);
    window.location.reload(true);
  };

  useEffect(() => {
    setCount(window.localStorage.getItem("itemCount"));
    if (localStorage.getItem("token")) {
      setIsLoggedIn(true);
    } else {
      setIsLoggedIn(false);
    }

    setCurrency(localStorage.getItem("currency"));
    //Fetching products for searchbar
    const data = { queryType: "searchBarOnly" };
    axios
      .post(`http://3.218.103.129:8080/api/products/search/products`, data)
      .then((res) => {
        console.log("header", res.data.data);
        setSearchItems(res.data.data);
      })
      .catch(() => {
        //handle error
      });
  }, []);

  useEffect(() => {
    //code for currency change everywhere
    axios
      .get(
        "https://v6.exchangerate-api.com/v6/50c1270a5b5fef48bfb29271/latest/USD"
      )
      .then((result) => {
        localStorage.setItem("AUD", result.data.conversion_rates.AUD);
        localStorage.setItem("EUR", result.data.conversion_rates.EUR);
        setCurrency(localStorage.getItem("currency"));
      })
      .catch(() => {
        //handle error
      });
  }, []);

  const loginCondition = isLoggedIn ? (
    <>
      <li>
        <Link to="/dashboard">
          <a href data-toggle="modal">
            Dashboard
          </a>
        </Link>
      </li>
      <li onClick={handleLogout}>
        <a href data-toggle="modal">
          Logout
        </a>
      </li>
    </>
  ) : (
    <li>
      <Link to="/account">
        <a href data-toggle="modal" data-target="#login-modal">
          Login
        </a>
      </Link>
    </li>
  );
  const searchBarProducts = searchDisplayItems.length
    ? searchDisplayItems.map((item, index) => (
        <Link  to={`/product?id=${item._id}`}>
          <li>
            <img src={item.image} /> <span>{item.productName}</span>
          </li>
        </Link>
      ))
    : null;

  return (
    <div>
      <>
        {/* Start header section */}
        <header id="aa-header">
          {/* start header top  */}
          <div className="aa-header-top">
            <div className="container">
              <div className="row">
                <div className="col-md-12">
                  <div className="aa-header-top-area">
                    {/* start header top left */}
                    <div className="aa-header-top-left">
                      {/* start language */}
                      {shouldDisplay ? (
                        <div className="aa-language">
                          <select
                            onChange={(e) =>
                              handleCurrencyChange(e.target.value)
                            }
                            value={currency}
                          >
                            <option value="USD">USD</option>
                            <option value="AUD">AUD</option>
                            <option value="EUR">EUR</option>
                          </select>
                        </div>
                      ) : null}
                      {/* <div className="aa-language">
                        <select
                          onChange={(e) => handleCurrencyChange(e.target.value)}
                          value={currency}
                        >
                          <option value="USD">USD</option>
                          <option value="AUD">AUD</option>
                          <option value="EUR">EUR</option>
                        </select>
                      </div> */}
                      {/* / language */}
                      {/* start currency */}

                      {/* / currency */}
                      {/* start cellphone */}
                      <div className="cellphone hidden-xs">
                        <p>
                          <span className="fa fa-phone" />
                          00-62-658-658
                        </p>
                      </div>
                      {/* / cellphone */}
                    </div>
                    {/* / header top left */}
                    <div className="aa-header-top-right">
                      <ul className="aa-head-top-nav-right">
                        {/* <li>
                          <Link href="/account">
                            <a>My Account</a>
                          </Link>
                        </li> */}

                        <li className="hidden-xs">
                          <Link to="/cart">
                            My Cart
                          </Link>
                        </li>
                        <li className="hidden-xs">
                          <Link to="/checkout"> Checkout</Link>
                        </li>

                        {loginCondition}
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* / header top  */}
          {/* start header bottom  */}
          <div className="aa-header-bottom">
            <div className="container">
              <div className="row">
                <div className="col-md-12">
                  <div className="aa-header-bottom-area">
                    {/* logo  */}
                    <div className="aa-logo">
                      {/* Text based logo */}
                      <a>
                        <span className="fa fa-shopping-cart" />
                        <p>
                          <Link to="/">KeyKays </Link>
                          <strong></strong> <span>Coupon Store</span>{" "}
                        </p>
                      </a>
                      {/* img based logo */}
                      {/* <a href="index.html"><img src="img/logo.jpg" alt="logo img"></a> */}
                    </div>
                    {/* / logo  */}
                    {/* cart box */}
                    <div className="aa-cartbox">
                      <Link to="/cart">
                        <a className="aa-cart-link">
                          <span className="fa fa-shopping-basket" />
                          <span className="aa-cart-title">SHOPPING CART</span>
                          {/* <span className="aa-cart-notify"></span> */}
                        </a>
                      </Link>
                    </div>
                    {/* / cart box */}
                    {/* search box */}
                    <div className="aa-search-box">
                      <input
                        type="text"
                        name
                        id
                        onChange={(e) => handleSearchQuery(e.target.value)}
                        placeholder="Search here ex. 'Netflix' "
                      />

                      <ul
                        className="search__items"
                        onClick={() => setSearchDisplayItems([])}
                      >
                        {searchBarProducts}
                      </ul>

                      <button>
                        <span className="fa fa-search" />
                      </button>
                    </div>

                    {/* / search box */}
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* / header bottom  */}
        </header>
        {/* / header section */}
        {/* menu */}
        <section
          id="menu"
          style={{
            position: "sticky",
            top: "0",
            zIndex: "999",
          }}
        >
          <div className="container">
            <div className="menu-area">
              {/* Navbar */}
              <div className="navbar navbar-default" role="navigation">
                <div className="navbar-header">
                  <button
                    type="button"
                    className="navbar-toggle"
                    data-toggle="collapse"
                    data-target=".navbar-collapse"
                  >
                    <span className="sr-only">Toggle navigation</span>
                    <span className="icon-bar" />
                    <span className="icon-bar" />
                    <span className="icon-bar" />
                  </button>
                </div>
                <div className="navbar-collapse collapse">
                  {/* Left nav */}
                  <ul className="nav navbar-nav">
                    <li>
                      <Link to="/">
                        <a>Home</a>
                      </Link>
                    </li>
                    <li>
                      <a href="#">
                        Software Keys <span className="caret" />
                      </a>
                      <ul className="dropdown-menu">
                        <li>
                          <Link to="/category?tag=ms-office">Ms-Office</Link>
                        </li>
                        <li>
                          <Link to="/category?tag=windows">Windows</Link>
                        </li>
                        <li>
                          <Link to="/category?tag=netflix">
                            <a>Netflix</a>
                          </Link>
                        </li>
                        <li></li>
                        <li>
                          <a href="#">
                            Others <span className="caret" />
                          </a>
                          <ul className="dropdown-menu">
                            {/* <li>
                              <a href="#">Sleep Wear</a>
                            </li>
                            <li>
                              <a href="#">Sandals</a>
                            </li>
                            <li>
                              <a href="#">Loafers</a> */}
                            {/* </li> */}
                          </ul>
                        </li>
                      </ul>
                    </li>
                    <li>
                      <a href="#">
                        Lifetime Deals <span className="caret" />
                      </a>
                      {/* <ul className="dropdown-menu">
                        <li>
                          <a href="#">Kurta &amp; Kurti</a>
                        </li>
                        <li>
                          <a href="#">Trousers</a>
                        </li>
                        <li>
                          <a href="#">Casual</a>
                        </li>
                        <li>
                          <a href="#">Sports</a>
                        </li>
                        <li>
                          <a href="#">Formal</a>
                        </li>
                        <li>
                          <a href="#">Sarees</a>
                        </li>
                        <li>
                          <a href="#">Shoes</a>
                        </li>
                        <li>
                          <a href="#">
                            And more.. <span className="caret" />
                          </a>
                          <ul className="dropdown-menu">
                            <li>
                              <a href="#">Sleep Wear</a>
                            </li>
                            <li>
                              <a href="#">Sandals</a>
                            </li>
                            <li>
                              <a href="#">Loafers</a>
                            </li>
                            <li>
                              <a href="#">
                                And more.. <span className="caret" />
                              </a>
                              <ul className="dropdown-menu">
                                <li>
                                  <a href="#">Rings</a>
                                </li>
                                <li>
                                  <a href="#">Earrings</a>
                                </li>
                                <li>
                                  <a href="#">Jewellery Sets</a>
                                </li>
                                <li>
                                  <a href="#">Lockets</a>
                                </li>
                                <li className="disabled">
                                  <a className="disabled" href="#">
                                    Disabled item
                                  </a>
                                </li>
                                <li>
                                  <a href="#">Jeans</a>
                                </li>
                                <li>
                                  <a href="#">Polo T-Shirts</a>
                                </li>
                                <li>
                                  <a href="#">SKirts</a>
                                </li>
                                <li>
                                  <a href="#">Jackets</a>
                                </li>
                                <li>
                                  <a href="#">Tops</a>
                                </li>
                                <li>
                                  <a href="#">Make Up</a>
                                </li>
                                <li>
                                  <a href="#">Hair Care</a>
                                </li>
                                <li>
                                  <a href="#">Perfumes</a>
                                </li>
                                <li>
                                  <a href="#">Skin Care</a>
                                </li>
                                <li>
                                  <a href="#">Hand Bags</a>
                                </li>
                                <li>
                                  <a href="#">Single Bags</a>
                                </li>
                                <li>
                                  <a href="#">Travel Bags</a>
                                </li>
                                <li>
                                  <a href="#">Wallets &amp; Belts</a>
                                </li>
                                <li>
                                  <a href="#">Sunglases</a>
                                </li>
                                <li>
                                  <a href="#">Nail</a>
                                </li>
                              </ul>
                            </li>
                          </ul>
                        </li>
                      </ul> */}
                    </li>
                    <li>
                      <a href="#">
                        Gift Cards <span className="caret" />
                      </a>
                      <ul className="dropdown-menu">
                        <li>
                          <a href="#">Casual</a>
                        </li>
                        <li>
                          <a href="#">Sports</a>
                        </li>
                        <li>
                          <a href="#">Formal</a>
                        </li>
                        <li>
                          <a href="#">Standard</a>
                        </li>
                        <li>
                          <a href="#">T-Shirts</a>
                        </li>
                        <li>
                          <a href="#">Shirts</a>
                        </li>
                        <li>
                          <a href="#">Jeans</a>
                        </li>
                        <li>
                          <a href="#">Trousers</a>
                        </li>
                        <li>
                          <a href="#">
                            And more.. <span className="caret" />
                          </a>
                          <ul className="dropdown-menu">
                            <li>
                              <a href="#">Sleep Wear</a>
                            </li>
                            <li>
                              <a href="#">Sandals</a>
                            </li>
                            <li>
                              <a href="#">Loafers</a>
                            </li>
                          </ul>
                        </li>
                      </ul>{" "}
                    </li>
                    <li>
                      <a href="#">Subscriptions</a>
                    </li>
                    {/* <li>
                      <a href="#">
                        Digital <span className="caret" />
                      </a>
                      <ul className="dropdown-menu">
                        <li>
                          <a href="#">Camera</a>
                        </li>
                        <li>
                          <a href="#">Mobile</a>
                        </li>
                        <li>
                          <a href="#">Tablet</a>
                        </li>
                        <li>
                          <a href="#">Laptop</a>
                        </li>
                        <li>
                          <a href="#">Accesories</a>
                        </li>
                      </ul>
                    </li>
                    <li>
                      <a href="#">Furniture</a>
                    </li>
                    <li>
                      <a href="blog-archive.html">
                        Blog <span className="caret" />
                      </a>
                      <ul className="dropdown-menu">
                        <li>
                          <a href="blog-archive.html">Blog Style 1</a>
                        </li>
                        <li>
                          <a href="blog-archive-2.html">Blog Style 2</a>
                        </li>
                        <li>
                          <a href="blog-single.html">Blog Single</a>
                        </li>
                      </ul>
                    </li>
                    <li>
                      <a href="contact.html">Contact</a>
                    </li>
                    {/* <li>
                      <a href="#">
                        Pages <span className="caret" />
                      </a>
                      <ul className="dropdown-menu">
                        <li>
                          <a href="product.html">Shop Page</a>
                        </li>
                        <li>
                          <a href="product-detail.html">Shop Single</a>
                        </li>
                        <li>
                          <a href="404.html">404 Page</a>
                        </li>
                      </ul>
                    </li> */}
                  </ul>
                </div>
                {/*/.nav-collapse */}
              </div>
            </div>
          </div>
        </section>

        {/* / menu */}
      </>
    </div>
  );
}
