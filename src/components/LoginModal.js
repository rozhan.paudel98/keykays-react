import React from 'react'

export default function LoginModal() {
    return (
        <div>
            <>
  {/* Login Modal */}
  <div
    className="modal fade"
    id="login-modal"
    tabIndex={-1}
    role="dialog"
    aria-labelledby="myModalLabel"
    aria-hidden="true"
  >
    <div className="modal-dialog">
      <div className="modal-content">
        <div className="modal-body">
          <button
            type="button"
            className="close"
            data-dismiss="modal"
            aria-hidden="true"
          >
            ×
          </button>
          <h4>Login or Register</h4>
          <form className="aa-login-form" action>
            <label htmlFor>
              Username or Email address<span>*</span>
            </label>
            <input type="text" placeholder="Username or email" />
            <label htmlFor>
              Password<span>*</span>
            </label>
            <input type="password" placeholder="Password" />
            <button className="aa-browse-btn" type="submit">
              Login
            </button>
            <label htmlFor="rememberme" className="rememberme">
              <input type="checkbox" id="rememberme" /> Remember me{" "}
            </label>
            <p className="aa-lost-password">
              <a href="#">Lost your password?</a>
            </p>
            <div className="aa-register-now">
              Don't have an account?<a href="account.html">Register now!</a>
            </div>
          </form>
        </div>
      </div>
      {/* /.modal-content */}
    </div>
    {/* /.modal-dialog */}
  </div>
</>

        </div>
    )
}
