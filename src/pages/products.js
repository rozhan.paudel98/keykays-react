import React, { Component, useState, useEffect } from "react";
import { useRouter, withRouter } from "next/router";
import axios from "axios";
import Link from "next/link";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Header from "../components/Header";

const Products = withRouter((props) => {
  const router = useRouter();
  console.log(props.router.query.tag);
  const [products, setProducts] = useState([]);

  const [query, setQuery] = useState("");
  const [cartItems, setCartItems] = useState([]);
  const [currencyIndex, setCurrencyIndex] = useState();
  const [AUD, setAUD] = useState(1);
  const [EUR, setEUR] = useState(1);

  const handleCart = async (product) => {
    await setCartItems([...cartItems, product]);
    console.log(cartItems);
    toast.success("Product added to cart");
  };

  //Component Did Mount
  useEffect(() => {
    const prevItems = localStorage.getItem("cartItems");
    console.log(prevItems);

    sessionStorage.setItem("cartItems", prevItems);
    setCartItems(JSON.parse(prevItems));

    setAUD(localStorage.getItem("AUD"));
    setEUR(localStorage.getItem("EUR"));
    setCurrencyIndex(localStorage.getItem("currency"));
  }, []);

  //Unique Items here
  useEffect(() => {
    let cartThings = [];
    if (cartItems) cartThings = getUniqueCartItems();

    sessionStorage.setItem("cartItems", JSON.stringify(cartThings));
    localStorage.setItem("itemCount", cartThings.length);
  }, [cartItems]);

  function onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
  }
  const getUniqueCartItems = () => {
    const unique = cartItems.filter(onlyUnique);
    return unique;
  };

  setTimeout(() => {
    setQuery(router.query.tag);
  }, 1000);

  useEffect(() => {
    axios
      .post(`${process.env.backendEndPoint}/api/products/fetch/products/tag`, {
        tag: props.router.query.tag,
      })
      .then((result) => {
        setProducts(result.data.data);
      });
  }, [query]);

  return (
    <div>
      <Header shouldDisplay={true} />
      <section>
        <ToastContainer />
        <div className="container__1" style={{ height: "100vh" }}>
          <div className="row">
            <div className="col-md-12">
              <div
                className="checkout-area"
                style={{
                  marginLeft: "20vw",
                }}
              >
                <h3>Products</h3>
                {products.map((product, index) => {
                  return (
                    <div class="content__card1 col-md-4 col-lg-3">
                      <Link
                        href={"/product/[id]"}
                        as={`/product/${product._id}`}
                      >
                        <img src={product.image} />
                      </Link>
                      <h3>{product.productName}</h3>
                      <p>{product.description}</p>
                      <h6>
                        {currencyIndex == "EUR"
                          ? "EUR" + product.price.productPrice * EUR
                          : currencyIndex == "AUD"
                          ? "AUD" + product.price.productPrice * AUD
                          : "USD" + product.price.productPrice}
                      </h6>
                      <ul>
                        <li>
                          <i class="fa fa-star" aria-hidden="true"></i>
                        </li>
                        <li>
                          <i class="fa fa-star" aria-hidden="true"></i>
                        </li>
                        <li>
                          <i class="fa fa-star" aria-hidden="true"></i>
                        </li>
                        <li>
                          <i class="fa fa-star" aria-hidden="true"></i>
                        </li>
                        <li>
                          <i class="fa fa-star" aria-hidden="true"></i>
                        </li>
                      </ul>
                      <button class="buy-2" onClick={() => handleCart(product)}>
                        Add to Cart
                      </button>
                    </div>
                  );
                })}
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
});

export default Products;
