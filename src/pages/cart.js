import { Link } from "react-router-dom";

import React, { useEffect, useState } from "react";
import Header from "../components/Header";
import {Helmet} from "react-helmet";

export default function Cart() {

  const [cartItems, setCartItems] = useState([]);
  const [total, setTotal] = useState(0);
  const [currencyIndex, setCurrencyIndex] = useState();
  const [AUD, setAUD] = useState(1);
  const [EUR, setEUR] = useState(1);

  useEffect(() => {

    setCartItems(JSON.parse(sessionStorage.getItem("cartItems")));
    localStorage.setItem("cartItems", sessionStorage.getItem("cartItems"));

    setAUD(localStorage.getItem("AUD"));
    setEUR(localStorage.getItem("EUR"));
    setCurrencyIndex(localStorage.getItem("currency"));
  }, []);

  useEffect(() => {
    calculateTotal(cartItems);
  }, [cartItems]);

  //remove cart item
  const removeCartItem = (i) => {
    console.log(i);
    var newCartItems = JSON.parse(sessionStorage.getItem("cartItems"));
    console.log(newCartItems, "Cart items");
    newCartItems.splice(i, 1);
    console.log("after slice", newCartItems);
    sessionStorage.setItem("cartItems", JSON.stringify(newCartItems));
    localStorage.setItem("cartItems", JSON.stringify(newCartItems));
    setCartItems(newCartItems);
  };

  const calculateTotal = (items) => {
    var totalValue = 0;
    items.forEach((item) => {
      totalValue = totalValue + parseInt(item.price.productPrice);
    });
    setTotal(totalValue);
  };

  return (
    <div>
      <>
      <Helmet>
        <title>Cart</title>
        <meta name="description" content="Helmet application" />
        <script type="text/javascript" src="js/nouislider.js" defer></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js" defer></script>

    <script src="js/bootstrap.js" defer></script>

    <script type="text/javascript" src="js/jquery.smartmenus.js" defer></script>

    <script
      type="text/javascript"
      src="js/jquery.smartmenus.bootstrap.js"
    defer
    ></script>


    <script
      type="text/javascript"
      src="js/jquery.simpleGallery.js"
      defer
    ></script>
    <script type="text/javascript" src="js/jquery.simpleLens.js" defer></script>
  

    <script type="text/javascript" src="js/nouislider.js" defer></script>

    <script src="js/custom.js" defer></script>
    </Helmet>
        <Header shouldDisplay={true} />
        {/* catg header banner section */}
        <section id="aa-catg-head-banner">
          <img src="img/fashion/fashion-header-bg-8.jpg" alt="fashion img" />
          <div className="aa-catg-head-banner-area">
            <div className="container">
              <div className="aa-catg-head-banner-content">
                <h2>Cart Page</h2>
                <ol className="breadcrumb">
                  <li>
                    <a href="index.html">Home</a>
                  </li>
                  <li className="active">Cart</li>
                </ol>
              </div>
            </div>
          </div>
        </section>
        {/* / catg header banner section */}
        {/* Cart view section */}
        <section id="cart-view">
          <div className="container">
            <div className="row">
              <div className="col-md-12">
                <div className="cart-view-area">
                  <div className="cart-view-table">
                    <form action>
                      <div className="table-responsive">
                        <table className="table">
                          <thead>
                            <tr>
                              <th />
                              <th />
                              <th>Product</th>
                              <th>Price</th>
                              {/* <th>Quantity</th> */}
                              <th>Total</th>
                            </tr>
                          </thead>
                          <tbody>
                            {cartItems.map((item, i) => {
                              return (
                                <>
                                  <tr>
                                    <td>
                                      <a
                                        className="remove"
                                        onClick={() => {
                                          removeCartItem(i);
                                        }}
                                      >
                                        <fa className="fa fa-close" />
                                      </a>
                                    </td>
                                    <td>
                                      <a href="#">
                                        <img src={item.image} alt="img" />
                                      </a>
                                    </td>
                                    <td>
                                      <a className="aa-cart-title" href="#">
                                        {item.productName}
                                      </a>
                                    </td>
                                    <td>
                                      {currencyIndex == "EUR"
                                        ? "EUR " +
                                          (
                                            item.price.productPrice * EUR
                                          ).toFixed(4)
                                        : currencyIndex == "AUD"
                                        ? "AUD " +
                                          (
                                            item.price.productPrice * AUD
                                          ).toFixed(4)
                                        : "USD " + item.price.productPrice}
                                    </td>
                                    {/* <td>
                                      <input
                                        className="aa-cart-quantity"
                                        type="number"
                                        defaultValue={1}
                                      />
                                    </td> */}
                                    <td>
                                      {currencyIndex == "EUR"
                                        ? "EUR " +
                                          (
                                            item.price.productPrice * EUR
                                          ).toFixed(4)
                                        : currencyIndex == "AUD"
                                        ? "AUD " +
                                          (
                                            item.price.productPrice * AUD
                                          ).toFixed(4)
                                        : "USD " + item.price.productPrice}
                                    </td>
                                  </tr>
                                </>
                              );
                            })}
                          </tbody>
                        </table>
                      </div>
                    </form>
                    {/* Cart Total view */}
                    <div className="cart-view-total">
                      <h4>Cart Totals</h4>
                      <table className="aa-totals-table">
                        <tbody>
                          <tr>
                            <th>Vat</th>
                            <td>$0</td>
                          </tr>
                          <tr>
                            <th>Total</th>
                            <td>
                              {currencyIndex == "EUR"
                                ? "EUR " + (total * EUR).toFixed(4)
                                : currencyIndex == "AUD"
                                ? "AUD " + (total * AUD).toFixed(4)
                                : "USD " + total}
                            </td>
                          </tr>
                        </tbody>
                      </table>
                      <Link to="/checkout">
                        <a className="aa-cart-view-btn">Proced to Checkout</a>
                      </Link>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        {/* / Cart view section */}
      </>
    </div>
  );
}
