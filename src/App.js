import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import "./home.css"
import "./globals.css"
import Footer from './components/Footer';
import Header from './components/Header';
import HomePage from './pages/HomePage';
import Cart from './pages/cart';
import SingleProduct from './pages/SingleProduct';
import Checkout from './pages/checkout';
import CategoryPage from './pages/CategoryPage';
import Account from './pages/account';
import UserDashboard from './pages/userdashboard';
import NotFound from './pages/NotFound';

// Protected Route
const ProtectedRoute = ({ component: Component, ...props }) => {
  return (
    <Route
      {...props}
      render={
        (props) =>
          localStorage.getItem('token') ? (
            <>
              <Component {...props}></Component>
            </>
          ) : (
            <Redirect to='/'></Redirect>
          ) // TODO send props
      }
    />
  );
};

function App() {
  return (
    <div>
      <ToastContainer
        position='top-right'
        autoClose={1300}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover={false}
      />
      <Router>
        <Switch>
          <Route path='/' exact component={HomePage} />
          <Route path='/cart' exact component={Cart} />
          <Route path='/product' component={SingleProduct} />
          <Route path='/checkout' component={Checkout} />
          <Route path='/category' component={CategoryPage} />
          <Route path='/register' component={Account} />
          <Route path='/login' component={Account} />
          <Route path='/account' component={Account} />
          <ProtectedRoute path='/dashboard' component={UserDashboard} />
     
          <Route component={NotFound} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
